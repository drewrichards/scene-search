<?php

namespace App\Sources\Shows;

use App\Contracts\ShowSource;
use App\Models\Band;
use Carbon\Carbon;

class OhMyRockness implements ShowSource
{
    const VENUES = [
        [
            'name' => 'Mercury Lounge',
            'url' => 'https://www.ohmyrockness.com/venues/mercury-lounge',
        ],
        [
            'name' => 'Baby\'s All Right',
            'url' => 'https://www.ohmyrockness.com/venues/baby-s-all-right',
        ],
        [
            'name' => 'Knitting Factory Brooklyn',
            'url' => 'https://www.ohmyrockness.com/venues/knitting-factory-brooklyn',
        ],
        [
            'name' => 'ALPHAVILLE',
            'url' => 'https://www.ohmyrockness.com/venues/alphaville',
        ],
        [
            'name' => 'Muchmore\'s',
            'url' => 'https://www.ohmyrockness.com/venues/muchmore-s',
        ],
        [
            'name' => 'Rough Trade',
            'url' => 'https://www.ohmyrockness.com/venues/rough-trade',
        ],
        [
            'name' => 'Sunnyvale',
            'url' => 'https://www.ohmyrockness.com/venues/sunnyvale',
        ],
        [
            'name' => 'Union Pool',
            'url' => 'https://www.ohmyrockness.com/venues/union-pool',
        ],
        [
            'name' => 'Goldsounds',
            'url' => 'https://www.ohmyrockness.com/venues/goldsounds',
        ],
        [
            'name' => 'Matchless',
            'url' => 'https://www.ohmyrockness.com/venues/matchless',
        ],
        [
            'name' => 'Berlin',
            'url' => 'https://www.ohmyrockness.com/venues/berlin',
        ],
    ];

    /**
     * Imports shows for all venues
     */
    public function import()
    {
        foreach (self::VENUES as $venue) {
            $this->importFromVenue($venue);
        }
    }

    /**
     * Imports all the shows for a venue
     *
     * @param $venue
     */
    protected function importFromVenue($venue)
    {
        $page = file_get_contents($venue['url']);
        $shows = $this->parsePage($page);
        foreach ($shows as $show) {
            $this->importShow($venue, $show);
        }
    }

    /**
     * Imports show and band data into the database
     *
     * @param $venue
     * @param $show
     */
    protected function importShow($venue, $show)
    {
        $newShow = [
            'venue' => $venue['name'],
            'show_date' => new Carbon($show->startDate),
        ];

        $bands = array_map('trim', explode(',', $show->name));
        foreach ($bands as $bandName) {
            $band = Band::findOrCreate($bandName);
            if (!$band->isDuplicateShow($venue['name'])) {
                $band->shows()->create($newShow);
            }
        }
    }

    /**
     * Parses the show data from an html page
     *
     * @param $page
     * @return mixed|null
     */
    protected function parsePage($page)
    {
        libxml_use_internal_errors(true); // Allow imperfect HTML

        $doc = new \DOMDocument();
        $doc->loadHTML($page, LIBXML_NOWARNING);
        $scripts = $doc->getElementsByTagName('script');
        foreach ($scripts as $script) {
            if ($script->getAttribute('type') === 'application/ld+json') {
                return json_decode($script->nodeValue);
            }
        }

        return null;
    }
}
